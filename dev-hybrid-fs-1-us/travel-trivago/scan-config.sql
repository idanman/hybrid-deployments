start transaction;		/* NEVER REMOVE THIS LINE!!! */

REPLACE INTO ScanManager.tbl_scan_configs (scan_group) VALUES ('travel-trivago');
UPDATE ScanManager.tbl_scan_configs SET
	description = 'Travel_trivago',

	is_active = 1,
	scan_type = 'full',
	scan_pages = 200,
	contact_emails = 'sparks-travel-prod@fornova.net',

	when_minute = '0',
	when_hour = '25',
	when_day_of_month = '*',
	when_month = '*',
	when_day_of_week = '*',
	-- custom_schedule_cmd are operations that accoured in each trigger, not related to the scan.
	-- Command to be executed before the scan:
	pre_scan_cmd='python2.7 /usr/bin/cmstools ota cu -o 1398 -m trivago -S Travel_metas --continue-on-failed-location -V',		-- IMPORTANT! In case you need to run an SQL code that updates something, use mysql -e. For example: pre_scan_cmd='mysql -e "UPDATE ....."'

	-- Ontology to run custom_schedule_cmd on. Set to 0 to run on small firestarter or to 1 to run on BIG firestarter.
	-- NOTICE: In effect only if custom_schedule_cmd_type = 'SH'
	-- IMPORTANT: All clients that have more then 15k records should have their exports running on BIG firestarter.
	schedule_ontology = 0,


	export = 1,
	-- Ontology to run export on. Set to 0 to run on small firestarter or to 1 to run on BIG firestarter.
	-- IMPORTANT: All clients that have more then 15k records should have their exports running on BIG firestarter.
	export_ontology = 1,
	custom_export_cmd = '',
	custom_export_cmd_type = 'SH',
	upload = 1,
	auto_from_backup=0,
	alert = 1,
	purge_records = 1,

	/*********************************
   		Advanced flags 
	*********************************/

	-- Set to one to enable debug for scheduled UDM jobs
	-- WARNING: Does not work if you schedule through custom_schedule_cmd,
	--          UNLESS, you use data2master to schedule the scan
	debug = 0,

	-- This hook will be run when all FULL jobs from scan have finished (if scan is full). Hook is an SQL code.
	-- @scan_id@ string in the hook will be replaced with current scan_id
	-- post_full_scan_hook = 'CALL eCom_EN_trivago_agoda_poc_4.post_full_scan_hook(@scan_id@)',
	post_full_scan_hook = '',

	-- This hook will be run when all jobs from scan have finished. Hook is an SQL code.
	post_scan_hook = '',

	-- Set this to 0 to disable check for not-started scans
	-- I.e. if disabled, the system will NOT insist on scheduling scan if it missed its start time
	alert_on_not_started = 1,

	-- Set to 1 if you do not want the system to create a scan for you. 
    -- For example if you have data2master that schedules detailed scan afterwards
	dont_create_scan = 0,

	-- Set to 1 to force created scan have export_policy=dontExport
	-- Useful for data2masters that run export themselves
	disable_export_for_scan = 0,

	parent_scan_group="travel-metas",
	details_retry_timeout_in_minutes = 60,
	customer_name='dev'
WHERE scan_group='travel-trivago';

commit;		/* NEVER REMOVE THIS LINE!!! */
