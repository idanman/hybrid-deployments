/* ****************************************************************  */
/* ATTANTION!!! THIS FILE IS NOT SUPPOSED TO BE MODIFIED MANUALLY!   */
/* ----------------------------------------------------------------  */
/* If you change UMI_id manually, anyway, you must remove the record */
/* with the old UMI_id from MySQL table in production by yourself!!! */
/*                                   Please do it very carefully...  */
/* ****************************************************************  */
REPLACE INTO ScanManager.tbl_UMI_ontologies
(UMI_id,		    ontology_id,is_active,	firestarter_enabled,	max_bandwidth) VALUES
('ecom-en-travel-agoda',	1398,		1,			'yes',		220);
