start transaction;		/* NEVER REMOVE THIS LINE!!! */


-- WATCH OUT!
-- THERE IS A BUG IN SCAN-MANAGER THAT MAKES THE AUTO-BACKUP AND PURGE COMMANDS
-- RUN ON THE FIRST LINE ONLY.
-- HENCE, IF YOU HAVE SEVERAL SITES FROM THE SAME ONTOLOGY, DON'T CREATE SEVERAL LINES.
-- INSTEAD, USE ONE LINE, AND IN FIELD 'value' specify all the sites (comma separated).
DELETE FROM ScanManager.tbl_scan_groups WHERE scan_group='tripadvisor-for-marriott-rfp';
REPLACE INTO ScanManager.tbl_scan_groups
(scan_group,				is_active,	ontology_id,	type,		value) VALUES
('tripadvisor-for-marriott-rfp',	1,		1399,		'sites',	'1,3,5,6,9,10,11,12,16,17,18,19,22,24,25,31,36,38,39');

commit;		/* NEVER REMOVE THIS LINE!!! */
