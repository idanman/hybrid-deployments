start transaction;		/* NEVER REMOVE THIS LINE!!! */

REPLACE INTO ScanManager.tbl_UMIs (UMI_id) VALUES ("UDM2_travel");
UPDATE ScanManager.tbl_UMIs SET
	UMI_path = "@aws-large",
	auto_setup = 1,
	bandwidth = 15,
	udm_bandwidth = 15,
	qa_bandwidth = 0,
	cron_bandwidth = 0,
	debs = "UDM2_travel/udm_2.0.1.7_trusty.deb",
	description = "",
	bandwidth_reduce_timeout = 20
WHERE UMI_id="UDM2_travel";

commit;		/* NEVER REMOVE THIS LINE!!! */
