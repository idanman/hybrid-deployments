start transaction;		/* NEVER REMOVE THIS LINE!!! */
/*
 * NOTE: Whenever you need to use "%" in your SQL queries, use "%%" in instead.
 */
DELETE FROM ScanManager.tbl_exports WHERE scan_group='priceline-for-marriott-rfp';

/*
 * common
 */
REPLACE INTO ScanManager.tbl_exports
(scan_group,        export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,    digest, description) VALUES
('priceline-for-marriott-rfp',    'common',       10,         1,          'local',    1,      1,            0,        '',     'none', 'kmutils + production2customer');

DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='common';
INSERT INTO ScanManager.tbl_export_cmds
(scan_group,    export_id,  ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
('priceline-for-marriott-rfp',    'common',   5,          0,          'sh',       '',      'udmtools sn -n cbb -s 1437.1 -t Travel_OTA_priceline.tbl_master'),
('priceline-for-marriott-rfp',    'common',   10,         1,          'sh',       '',      'echo y | kmutils ed 1437.eCom_EN'),
('priceline-for-marriott-rfp',    'common',   20,         1,          'sh',       '',      'cmstools dnc p2c Travel_OTA_priceline -p tbl_production -c tbl_customer'),
('priceline-for-marriott-rfp',    'common',   30,         1,          'sql2csv',  'active-records.cnt',   'SELECT count(1) FROM Travel_OTA_priceline.tbl_customer');


REPLACE INTO ScanManager.tbl_exports
(scan_group,        export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,    digest, description) VALUES
('priceline-for-marriott-rfp',    'coverage_for_qa',       20,         0,          'local',    1,      0,            0,        '',     'none', '');

DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='coverage_for_qa';
INSERT INTO ScanManager.tbl_export_cmds
(scan_group,        export_id,           ordering,   is_active,  cmd_type, `outfile`,  cmd) VALUES
('priceline-for-marriott-rfp',    'coverage_for_qa',   10,         1,          'sh',     '',      'mysql -e "DROP TABLE IF EXISTS Travel_OTA_priceline.tbl_coverage_for_qa"'),
('priceline-for-marriott-rfp',    'coverage_for_qa',   20,         1,          'sh',     '',      'mysql -e "CREATE TABLE Travel_OTA_priceline.tbl_coverage_for_qa(id INTEGER UNSIGNED, site_id INTEGER UNSIGNED, first_update_timestamp timestamp, last_update_timestamp timestamp, breadcrumb TEXT, deep_link TEXT, original_data_pattern_text TEXT, KEY id_key(id), KEY site_id_key(site_id),sku TEXT,product_name TEXT,product_number TEXT)"'),
('priceline-for-marriott-rfp',    'coverage_for_qa',   30,         1,          'sh',     '',      'mysql -e "INSERT INTO Travel_OTA_priceline.tbl_coverage_for_qa SELECT id, site_id, first_update_timestamp, last_update_timestamp, agg_breadcrumb, deep_link, original_data_pattern_text,sku,product_name,product_number FROM Travel_OTA_priceline.tbl_production"'),
('priceline-for-marriott-rfp',    'coverage_for_qa',   40,         1,          'sh',     '',      'python -O /usr/bin/cmstools ecom-en esr -o 1437 -e 10 -V'),
('priceline-for-marriott-rfp',    'coverage_for_qa',   50,         1,          'sh',     '',      'udmtools q2csv -c "SELECT * FROM Travel_OTA_priceline.tbl_coverage_for_qa" | gzip > exploded_production.csv.gz'),
('priceline-for-marriott-rfp',    'coverage_for_qa',   60,         1,          'sh',     '',      'udmtools q2csv -c "SELECT breadcrumb, count(1) as number_of_products, group_concat(product_number) as product_numbers, group_concat(sku) as skus, group_concat(product_name) as product_names FROM Travel_OTA_priceline.tbl_coverage_for_qa group by breadcrumb order by breadcrumb" | gzip > exploded_breadcrumbs_histogram.csv.gz'),
('priceline-for-marriott-rfp',    'coverage_for_qa',   70,         1,          'sql2csv','active-records.cnt',   'SELECT count(1) FROM Travel_OTA_priceline.tbl_coverage_for_qa');

/*
 * ops OPS export
 */
REPLACE INTO ScanManager.tbl_exports
(scan_group,        export_id,      	ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,    digest, description) VALUES
('priceline-for-marriott-rfp',	'to-etl',	1000,		1,			'local',	1,		0,			  0,		's3://sparks-etl.fornova.net/marriott/hotel-level/',	'none',	'to-etl');

DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='to-etl';
INSERT INTO ScanManager.tbl_export_cmds
(scan_group,    	export_id,  		ordering,   is_active,  cmd_type,   `outfile`,  			cmd) VALUES
('priceline-for-marriott-rfp',	'to-etl',   10,         1,          'sh',       '',	'
scan_phase=$(echo "SELECT scan_phase FROM Travel_metas.tbl_marriott_scan_phase LIMIT 1" | mysql -N);
if [[ "$scan_phase" == "0" ]]; then
	reshopping_part=""
else
	reshopping_part="_reshopping_number_$scan_phase"
fi
udmtools q2csv -c "SELECT * FROM Travel_OTA_priceline.qry_customer_to_etl" | gzip > priceline_$(date "+%%Y%%m%%d-%%H%%M%%S")${reshopping_part}.csv.gz'),
('priceline-for-marriott-rfp',	'to-etl',   20,         1,          'sql2csv',  'active-records.cnt',   'SELECT count(1) FROM Travel_OTA_priceline.qry_customer_to_etl');

/*
 * Google mapping file.
 */
 REPLACE INTO ScanManager.tbl_exports
 (scan_group,   export_id,      ordering,   is_active,  method,     `lock`, show_stopper,   uri,                                                    digest, description) VALUES
('priceline-for-marriott-rfp',    'mapping-files',        900,        0,          'local',    1,      0,      's3p://feedshare.goldenfeeds.com/priceline-for-marriott-rfp/',   'none', 'Mapping file');

DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='mapping-files';
INSERT INTO ScanManager.tbl_export_cmds
(scan_group,    export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
('priceline-for-marriott-rfp',    'mapping-files',        10,         1,          'sh',       'google-mapping.csv.gz',        'udmtools q2csv -V -d "," -c "select * from Travel_OTA_priceline.qry_tbl_google_mapping" | gzip'),
('priceline-for-marriott-rfp',    'mapping-files',        30,         1,          'sql2csv',      'active-records.cnt',   'select count(1) from Travel_OTA_priceline.qry_tbl_google_mapping');


-- /*
--  * customer-exploded-table OPS export
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,			export_id,      				ordering,   is_active,  method,     `lock`, show_stopper,	zero_ok,	uri,    digest, description) VALUES
-- ('priceline-for-marriott-rfp',		'ops-exploded',		1010,		1,			'local',	1,		0,			  	0,			's3://tbl_customer-exports.goldenfeeds.com/priceline-for-marriott-rfp/',	'none',	'Export to S3 tbl_customer exploded export');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='ops-exploded';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,    	export_id,  				ordering,   is_active,  cmd_type,   `outfile`,  					cmd) VALUES
-- ('priceline-for-marriott-rfp',	'ops-exploded',  10,         1,          'sh',       'tbl_customer_exploded.csv.gz',	'udmtools q2csv -c "SELECT * FROM Travel_OTA_priceline.tbl_customer_exploded" | gzip'),
-- ('priceline-for-marriott-rfp',	'ops-exploded',  20,         1,          'sql2csv',  'active-records.cnt',   		'SELECT count(1) FROM Travel_OTA_priceline.tbl_customer_exploded');


-- /*
--  * Linkshare
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,     export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'linkshare',    20,         1,          'local',    1,      0,            0,  'ftp://@LS_USER@:@LS_PASS@@mftp.linksynergy.com',       'none', 'Export to Linkshare UK');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='linkshare';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'linkshare',    10,         1,          'sh',       '',                     'cmstools fg linkshare -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'linkshare',    20,         1,          'sh',       'active-records.cnt',   'tail -n +3 *nmerchandis*.txt | wc -l');
-- 
-- /*
--  * Ultimate Feed
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,     export_id,          ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                            digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'ultimate-feed',    30,         1,          'local',    1,      0,            0,  'ftp://fornova:F0Rn0v.fTp!@ftp.ultimatefeed.com/fornova/',      'none', 'Ultimate Feed export');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='ultimate-feed';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,          ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'ultimate-feed',    10,         1,          'sh',       'priceline-for-marriott-rfp.csv',        'udmtools q2csv -V -c "SELECT * FROM Travel_OTA_priceline.qry_ecom_ultimatefeed"'),
-- ('priceline-for-marriott-rfp', 'ultimate-feed',    20,         1,          'sh',       'active-records.cnt',   'tail -n +2 *.csv |wc -l');
-- 
-- /*
--  * CJ US
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group, export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'cj-us',        40,         1,          'local',    1,      0,        0,      'ftp://@CJ_USER@:@CJ_PASS@datatransfer.cj.com',     'none', 'Export to CJ US');

-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='cj-us';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group, export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp',		'cj-us',   		10,         1,          'sh',       '',      'udmtools data2master -N CJ_PromotionText -o 1437 -V');
-- ('priceline-for-marriott-rfp',		'cj-us',   		11,         1,          'sh',       '',      'udmtools data2master -N CJ_LinkTagging -o 1437 -V');
-- ('priceline-for-marriott-rfp',		'cj-us',   		12,         1,          'sh',       '',      'udmtools data2master -N CJ_ExclusionList -o 1437 -V');
-- ('priceline-for-marriott-rfp',		'cj-us',		20,         1,          'sh',       '',      'cmstools fg cj -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp',		'cj-us',		30,         1,          'sh',       'active-records.cnt',   'tail -n +7 *.csv |wc -l');
-- 
-- /*
--  * CJ UK
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,     export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,   uri,                                                digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'cj-uk',        50,         1,          'local',    1,      0,            0,  'ftp://@CJ_USER@:@CJ_PASS@datatransfer.cj.com',     'none', 'Export to CJ US');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='cj-uk';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp',		'cj-uk',   		10,         1,          'sh',       '',      'udmtools data2master -N CJ_PromotionText -o 1437 -V');
-- ('priceline-for-marriott-rfp',		'cj-uk',   		11,         1,          'sh',       '',      'udmtools data2master -N CJ_LinkTagging -o 1437 -V');
-- ('priceline-for-marriott-rfp',		'cj-uk',   		12,         1,          'sh',       '',      'udmtools data2master -N CJ_ExclusionList -o 1437 -V');
-- ('priceline-for-marriott-rfp',		'cj-uk',        20,         1,          'sh',       '',      'cmstools fg cj -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp',		'cj-uk',        30,         1,          'sh',       'active-records.cnt',   'tail -n +7 *.csv |wc -l');
-- 
-- /*
--  * Google
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,     export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                            digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'google',       60,         1,          'local',    1,      0,            0,   'ftp://@G_USER@:@G_PASS@@uploads.google.com',   'none', 'Export to Google');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='google';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'google',   10,         1,          'sh',       '',                     'cmstools fg grss -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'google',   20,         1,          'sh',       'active-records.cnt',   'grep \'<g:id>\' *.xml | wc -l');
-- 
-- /*
--  * Trade Doubler
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,     export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                        digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'tradedoubler', 70,         1,          'local',    1,      0,            0,  'ftp://@TD_USER@:@TD_PATH@@@data.tradedoubler.com',     'none', 'Export to TradeDoubler UK');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='tradedoubler';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group, export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'tradedoubler', 10,         1,          'sh',       '',                     'cmstools fg trade-doubler -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'tradedoubler', 20,         1,          'sh',       'active-records.cnt',   'grep \'<product id="\' *.xml |wc -l');
-- 
-- /*
--  * Affiliate Window
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,     export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                    digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'awin-uk',      80,         1,          'local',    1,      0,            0,  'ftp://@AW_USER@:@AW_PASS@@ftp.affiliatewindow.com',    'none', 'Export to AffiliateWindow UK');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='awin-uk';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,  ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'awin-uk',  10,         1,          'sh',       '',                     'cmstools fg aw -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'awin-uk',  20,         1,          'sh',       'active-records.cnt',   'grep "^EN," *.csv | wc -l');
-- 
-- /*
--  * OMG
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group, export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'omg',          90,         1,          'local',    1,      0,        0,      'sftp://@OMG_USER@:@OMG_PASS@@sftp.omgnetwork.co.uk',       'none', 'Export to OMG network UK');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='omg';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group, export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'omg',  10,         1,          'sh',       '',                     'cmstools fg omg -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'omg',  20,         1,          'sh',       'active-records.cnt',   'grep "<omg:pid>" *.xml |wc -l');
-- 
-- /*
--  * pepperjamNETWORK Feed
--  */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,     export_id,          ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'pepperjam',    100,        1,          'local',    1,      0,           0,   's3p://feedshare.goldenfeeds.com/priceline-for-marriott-rfp/',        'none', 'Pepper Jam export');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='pepperjam';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,          ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'pepperjam',    10,         1,          'sh',       '',                     'cmstools fg pjn -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'pepperjam',    20,         1,          'sh',       'active-records.cnt',   'tail -n +2 *.txt |wc -l');
-- 

/*
 * Linkshare XML
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,        export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                digest, description) VALUES
-- ('priceline-for-marriott-rfp',  'linkshare-xml',   120,         1,          'local',    1,      0,          0,    's3p://feedshare.goldenfeeds.com/priceline-for-marriott-rfp/',      'none', 'Export to Linkshare Merchandiser XML');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='netaporter' AND export_id='linkshare-xml';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,        export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp',  'linkshare-xml',    10,         1,          'sh',       '',                     'cmstools fg lsxml -o 1437 -m priceline-for-marriott-rfp -p @LS_PUBLISHER@'),
-- ('priceline-for-marriott-rfp',  'linkshare-xml',    20,         1,          'sh',       'active-records.cnt',   'grep -c product_id *mp.xml');


/*
 * Zanox
 */
--  REPLACE INTO ScanManager.tbl_exports
--  (scan_group,        export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                digest, description) VALUES
--  ('priceline-for-marriott-rfp',    'zanox',        130,            1,          'local',    1,      0,        0,      'ftp://@ZN_USER@:@ZN_PASS@@ftp.zanox.com',      'none', 'Export to Zanox');
-- 
--  DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='zanox';
--  INSERT INTO ScanManager.tbl_export_cmds
--  (scan_group,        export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
--  ('priceline-for-marriott-rfp',    'zanox',    10,         1,          'sh',       '',                     'cmstools fg zanox -o 1437 -m priceline-for-marriott-rfp'),
--  ('priceline-for-marriott-rfp',    'zanox',    20,         1,          'sh',       'active-records.cnt',   'tail -n +2 *ZANOX*.csv | wc -l');


/*
 * Microsoft/CIAO
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group, export_id,  ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                        digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'ciao',     140,        1,          'local',    1,      0,        0,      'ftp://@USER@:@PASS@@ftp.fornova.net',      'none', 'Export to CIAO/Microsoft');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='ciao';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,  export_id,     ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'ciao',         10,         1,          'sh',       '',                     'cmstools fg ciao -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'ciao',         20,         1,          'sh',       'active-records.cnt',   'tail -n +2 *.csv | wc -l');

/*
 * EasyFeed
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group, export_id,      ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                                    digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'easyfeed',     150,        1,          'local',    1,      0,        0,      'ftp://easyfeed:feedMeMore@easyfeed.goldenfeeds.com/',  'none', 'EasyFeed export');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='easyfeed';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group, export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'easyfeed',     10,         1,          'sh',       'efmaster_priceline-for-marriott-rfp.csv',     'udmtools q2csv -V -c "SELECT * FROM Travel_OTA_priceline.qry_ecom_efmaster"'),
-- ('priceline-for-marriott-rfp', 'easyfeed',     20,         1,          'sh',       'active-records.cnt',   'grep -E "^(45|39)," *.csv |wc -l');


/*
 * ShopStyle
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group, export_id,  ordering,   is_active,  method,     `lock`, show_stopper, zero_ok,  uri,                                        digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'shopstyle',        140,        1,          'local',    1,      0, 0,             's3p://feedshare.goldenfeeds.com/priceline-for-marriott-rfp/',        'none', 'Export shopstyle');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='shopstyle';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,  export_id,     ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'shopstyle',            10,         1,          'sh',       '',                     'cmstools fg shopstyle -o 1437 -m priceline-for-marriott-rfp'),
-- ('priceline-for-marriott-rfp', 'shopstyle',            20,         1,          'sh',       'active-records.cnt',   'tail -n +2 *.csv | wc -l');



/***************************************************************************************************
 * ShopNation consist of three exports: delta, image cache and actual cmstools fg
 ***************************************************************************************************/

/*
 * ShopNation - delta
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,      export_id,         ordering,   is_active,  method,    `lock`,  show_stopper, zero_ok,  uri,         digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'shopnation-delta', 150,        1,          'local',    1,      0,            0,  '', 	       'none', 'data2master shopnation INCREMENTAL');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='shopnation-delta';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,           ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'shopnation-delta',  10,         1,          'sh',       '',                     'udmtools data2master -N @MERCHANT@_shopnation -o 1437'),
-- ('priceline-for-marriott-rfp', 'shopnation-delta',  20,         1,          'sql2csv',  'active-records.cnt',   'SELECT count(1) FROM Travel_OTA_priceline.tbl_customer_shopnation_delta');

/*
 * ShopNation - image-cache
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,      export_id,               ordering,   is_active,  method,    `lock`,  show_stopper, zero_ok,  uri,         digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'shopnation-image-cache', 160,        1,          'local',    1,      0,            0,  '', 	       'none', 'Image caching tool');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='shopnation-image-cache';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,                 ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 'shopnation-image-cache',  10,         1,          'sh',       '',                     'python2.7 /usr/bin/cmstools ecom-en cache-urls -u "SELECT distinct large_image_url FROM Travel_OTA_priceline.tbl_customer_shopnation_delta" -j 10 -m @MERCHANT@ -V'),  /* -m <string> - string should be always the same as merchant name in tbl_feed_merchants */
-- ('priceline-for-marriott-rfp', 'shopnation-image-cache',  20,         1,          'sh',       'active-records.cnt',   'echo 0');

/*
 * ShopNation - cmstools fg (API)
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,      export_id,       ordering,   is_active,  method,    `lock`,  show_stopper, zero_ok,  uri,         digest, description) VALUES
-- ('priceline-for-marriott-rfp', 'shopnation-api', 170,        1,          'local',    1,      0,            0,  '', 	       'none', 'Product push to ShopNation API');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='shopnation-api';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,         ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- /* PRODUCTION COMMAND */
-- ('priceline-for-marriott-rfp', 'shopnation-api',  10,         1,          'sh',       '',                     'python2.7 /usr/bin/cmstools fg sn -o 1437 -m @MERCHANT@'),
-- /* TESTING COMMAND */
-- -- ('priceline-for-marriott-rfp', 'shopnation-api',  10,         1,          'sh',       '',                     'python2.7 /usr/bin/cmstools fg sn -o 1437 -m @MERCHANT@ --leave-workdir --update-api-endpoint=http://feedserver1.qa.shopnation.com/product/upload.html --delete-api-endpoint=http://feedserver1.qa.shopnation.com/product/unavailable.html'),
-- ('priceline-for-marriott-rfp', 'shopnation-api',  20,         1,          'sql2csv',  'active-records.cnt',   'SELECT count(1) FROM Travel_OTA_priceline.tbl_shopnation_export_log WHERE status="OK"');

/*
 * zmags
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,    export_id,      ordering,  is_active,  method,    `lock`,  show_stopper, zero_ok, uri,         digest, description) VALUES
-- ('priceline-for-marriott-rfp', 		'zmags', 		60,        1,          'local',    1,      0,   0,           '', 	       'none', 'Product push to zmags API');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='zmags';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,     export_id,      ordering,   is_active,  cmd_type,   `outfile`,              cmd) VALUES
-- ('priceline-for-marriott-rfp', 		'zmags', 	 10,         1,          'sql2csv',	 '', 				     'CALL ECOM_CREATE_ZMAGS_DELTA("Travel_OTA_priceline.tbl_customer_delta", "grandmaster_product_number")'),
-- ('priceline-for-marriott-rfp', 		'zmags', 	 20,         1,          'sh', 		 'priceline-for-marriott-rfp-zmags.xml',     'python2.7 /usr/bin/cmstools fg zmags -o 1437 -m @MERCHANT@ -g l1_gid,l2_gid -V'),
-- ('priceline-for-marriott-rfp', 		'zmags', 	 30,         1,          'sh', 		 'active-records.cnt',   'grep "<product_id>" *.xml |wc -l');


/*
 * opsite format
 */
-- REPLACE INTO ScanManager.tbl_exports
-- (scan_group,	export_id,		ordering,	is_active,	method,		`lock`,	show_stopper,	uri,													digest, description) VALUES
-- ('priceline-for-marriott-rfp',	'opsite', 		280,		1,			'local',	1,		0,				'sftp://opsite:thrill45me@opsite.goldenfeeds.com/incoming/@REPLACE_ME@/',	'none',	'opsite export');
-- 
-- DELETE FROM ScanManager.tbl_export_cmds WHERE scan_group='priceline-for-marriott-rfp' AND export_id='opsite';
-- INSERT INTO ScanManager.tbl_export_cmds
-- (scan_group,	export_id,		ordering,	is_active,	cmd_type,	`outfile`,				cmd) VALUES
-- ('priceline-for-marriott-rfp',	'opsite',		10,			1,			'sh',		'priceline-for-marriott-rfp.csv',		'udmtools q2csv -VV -d "," -c "SELECT * FROM Travel_OTA_priceline.tbl_customer"'),
-- ('priceline-for-marriott-rfp',	'opsite',		20,			1,			'sh',		'active-records.cnt',	'grep http:// |wc -l');

commit;		/* NEVER REMOVE THIS LINE!!! */
