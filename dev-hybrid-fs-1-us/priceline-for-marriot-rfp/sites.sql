start transaction;		/* NEVER REMOVE THIS LINE!!! */


-- WATCH OUT!
-- THERE IS A BUG IN SCAN-MANAGER THAT MAKES THE AUTO-BACKUP AND PURGE COMMANDS
-- RUN ON THE FIRST LINE ONLY.
-- HENCE, IF YOU HAVE SEVERAL SITES FROM THE SAME ONTOLOGY, DON'T CREATE SEVERAL LINES.
-- INSTEAD, USE ONE LINE, AND IN FIELD 'value' specify all the sites (comma separated).
DELETE FROM ScanManager.tbl_scan_groups WHERE scan_group='priceline-for-marriott-rfp';
REPLACE INTO ScanManager.tbl_scan_groups
(scan_group,			is_active,	ontology_id,	type,		value) VALUES
('priceline-for-marriott-rfp',	1,		1437,		'sites',	'1,6');

commit;		/* NEVER REMOVE THIS LINE!!! */