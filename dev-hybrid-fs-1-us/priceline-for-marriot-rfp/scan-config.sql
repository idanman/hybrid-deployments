start transaction;		/* NEVER REMOVE THIS LINE!!! */

REPLACE INTO ScanManager.tbl_scan_configs (scan_group) VALUES ('priceline-for-marriott-rfp');
UPDATE ScanManager.tbl_scan_configs SET
	description = 'Travel_OTA_priceline',

	is_active = 1,
	scan_type = 'detailed',
	scan_pages = 200,
	contact_emails = 'ecom-production@fornova.net',

	when_minute = '0',
	when_hour = '25',
	when_day_of_month = '*',
	when_month = '*',
	when_day_of_week = '*',
	-- custom_schedule_cmd are operations that accoured in each trigger, not related to the scan.
	-- Command to be executed before the scan:
	pre_scan_cmd='
		scan_id=$(echo "SELECT scan_id FROM ScanManager.tbl_jobs WHERE job_id=$CRON_JOB_ID" | mysql -N);
		mysql -e "UPDATE Travel_OTA_priceline.tbl_results_common set scan_id=$scan_id"',
	-- IMPORTANT! In case you need to run an SQL code that updates something, use mysql -e. For example: pre_scan_cmd='mysql -e "UPDATE ....."'

	-- Ontology to run custom_schedule_cmd on. Set to 0 to run on small firestarter or to 1 to run on BIG firestarter.
	-- NOTICE: In effect only if custom_schedule_cmd_type = 'SH'
	-- IMPORTANT: All clients that have more then 15k records should have their exports running on BIG firestarter.
	schedule_ontology = 0,


	export = 0,
	-- Ontology to run export on. Set to 0 to run on small firestarter or to 1 to run on BIG firestarter.
	-- IMPORTANT: All clients that have more then 15k records should have their exports running on BIG firestarter.
	export_ontology = 0,
	custom_export_cmd = '',
	custom_export_cmd_type = 'SH',
	upload = 0,
	alert = 0,
	auto_from_backup = 0,
	purge_records = 0,

	/*********************************
   		Advanced flags 
	*********************************/

	-- Set to one to enable debug for scheduled UDM jobs
	-- WARNING: Does not work if you schedule through custom_schedule_cmd,
	--          UNLESS, you use data2master to schedule the scan
	debug = 0,

	-- This hook will be run when all FULL jobs from scan have finished (if scan is full). Hook is an SQL code.
	-- @scan_id@ string in the hook will be replaced with current scan_id
	post_full_scan_hook = '', 

	-- This hook will be run when all jobs from scan have finished. Hook is an SQL code.
	post_scan_hook = '',

	-- Set this to 0 to disable check for not-started scans
	-- I.e. if disabled, the system will NOT insist on scheduling scan if it missed its start time
	alert_on_not_started = 1,

	-- Set to 1 if you do not want the system to create a scan for you. 
    -- For example if you have data2master that schedules detailed scan afterwards
	dont_create_scan = 0,

	-- Set to 1 to force created scan have export_policy=dontExport
	-- Useful for data2masters that run export themselves
	disable_export_for_scan = 0,
 	parent_scan_group="travel-marriott-rfp-parent",
	customer_name='customer_ovrs_marriott'
WHERE scan_group='priceline-for-marriott-rfp';


commit;		/* NEVER REMOVE THIS LINE!!! */
