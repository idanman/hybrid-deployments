#!/bin/bash

echo "                              vvvvvvvvvvvvvvvvvvvvv"
echo "T H I S   I S   H Y B R I D   I N T E G R A T I O N   F I R E S T A R T E R ! ! !"
echo "                              ^^^^^^^^^^^^^^^^^^^^^"
echo "*******************************************************************************"
echo ""

if [[ "$HOSTNAME" != "fornovia" ]] && [[ "$HOSTNAME" != "utopia" ]] ; then
	echo "--------------------"
	echo "ALARM ALARM ALARM!!!"
	echo "--------------------"
	echo "$0 is allowed to be run from <fornovia> or <utopia> only, while you are currently running it from <$HOSTNAME>"
	echo "Go to $(pwd) directory on one of allowed servers and try again."
	echo ""
	echo "OPERATION CANCELED!"
	exit 1
fi

if [[ "$1" == "-m" ]]; then
	if [[ "$2" == "" ]]; then
		echo "Please provide -m <SVN check-in message>"
		exit 1
	fi

	python2.7 /usr/bin/cmstools deploy source -m "$2"
	exit $?
fi

python2.7 /usr/bin/cmstools deploy source $*

