REPLACE INTO ScanManager.tbl_alerts_config
(id,	alert_definition_id,	description,										is_active,	scan_group, `when`,		priority,	params) VALUES
(1,		1,						'WARNING alert to send custom system messages',		1,			'system',	'custom',	'WARNING',	'{}'),
(2,		1,						'ERROR alert to send custom system messages',		1,			'system',	'custom',	'ERROR',	'{}'),
(3,		1,						'CRITICAL alert to send custom system messages',	1,			'system',	'custom',	'CRITICAL',	'{}'),
(4,		2,						'WARNING alert to send custom export messages',		1,			'system',	'custom',	'WARNING',	'{}'),
(5,		2,						'ERROR alert to send custom export messages',		1,			'system',	'custom',	'ERROR',	'{}'),
(6,		2,						'CRITICAL alert to send custom export messages',	1,			'system',	'custom',	'CRITICAL',	'{}');
