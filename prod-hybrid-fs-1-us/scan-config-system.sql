REPLACE INTO ScanManager.tbl_scan_configs (scan_group) VALUES ('system');
UPDATE ScanManager.tbl_scan_configs SET
	description = 'Reserved name. Do not use.',
	is_active = 0,
	scan_type = 'full',
	scan_pages = 0,
	contact_emails = '',
	when_minute = '*',
	when_hour = '*',
	when_day_of_month = '*',
	when_month = '*',
	when_day_of_week = '*',
	custom_schedule_cmd = '',
	export = 0,
	custom_export_cmd = '',
	upload = 0,
	alert = 1,
	purge_records = 0
WHERE scan_group='system';

