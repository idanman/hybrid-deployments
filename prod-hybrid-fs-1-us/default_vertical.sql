start transaction;              /* NEVER REMOVE THIS LINE!!! */

REPLACE INTO ScanManager.tbl_verticals (vertical_id) VALUES (1000);

UPDATE ScanManager.tbl_verticals SET
                        vertical_id = 1000,
                      vertical_name = 'Default Vertical',
                        ontology_id = 0,
                 detailed_scan_mode = 'perSite',
                   learn_scan_pages = 3,
                    full_scan_pages = 3,
                    fast_scan_pages = 3,
                detailed_scan_pages = 5,
                 garbage_scan_pages = 100,
                   number_of_trials = 3,
          number_of_detailed_trials = 2,
                          is_active = 0,
                max_learn_scan_jobs = 0,
                 max_full_scan_jobs = 0,
                 max_fast_scan_jobs = 0,
             max_detailed_scan_jobs = 0,
              max_garbage_scan_jobs = 0,
        learn_scan_priority_default = 400,
         full_scan_priority_default = 200,
         fast_scan_priority_default = 400,
     detailed_scan_priority_default = 300,
      garbage_scan_priority_default = 200,
                     retry_interval = 5,
parallel_detailed_jobs_per_vertical = 15,
    parallel_detailed_jobs_per_site = 1000,
     parallel_session_jobs_per_site = 1,
                             export = 0,
                alert_warning_email = 'hybrid-prod-support@fornova.com',
                  alert_error_email = 'hybrid-prod-support@fornova.com',
               alert_critical_email = 'hybrid-prod-support@fornova.com',
                    max_absence_age = 21,
                  wait_for_all_jobs = 1,
      enable_parallel_scans_support = 'NO'
WHERE vertical_id=1000;
;

commit;
	
