start transaction;		/* NEVER REMOVE THIS LINE!!! */


-- WATCH OUT!
-- THERE IS A BUG IN SCAN-MANAGER THAT MAKES THE AUTO-BACKUP AND PURGE COMMANDS
-- RUN ON THE FIRST LINE ONLY.
-- HENCE, IF YOU HAVE SEVERAL SITES FROM THE SAME ONTOLOGY, DON'T CREATE SEVERAL LINES.
-- INSTEAD, USE ONE LINE, AND IN FIELD 'value' specify all the sites (comma separated).
DELETE FROM ScanManager.tbl_scan_groups WHERE scan_group='ota_priceline_for_sn';
REPLACE INTO ScanManager.tbl_scan_groups
(scan_group,			is_active,	ontology_id,	type,		value		 ) VALUES
('ota_priceline_for_sn',	1,			1437,			'sites',	'1,2,3,4,6,7,8,10,11,12,16');

commit;		/* NEVER REMOVE THIS LINE!!! */
