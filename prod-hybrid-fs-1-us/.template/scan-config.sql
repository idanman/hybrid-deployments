start transaction;		/* NEVER REMOVE THIS LINE!!! */

REPLACE INTO ScanManager.tbl_scan_configs (scan_group) VALUES ('@SCAN_GROUP@');
UPDATE ScanManager.tbl_scan_configs SET
        customer_name='customer_ovrs_supernova_prod',
        is_active = 1,
        scan_type = 'detailed',
        scan_pages = 1,
        when_hour = '25',
        export = 0,
        upload = 0,
        alert = 0,
        purge_records = 1,
        debug = 10,
        auto_from_backup = 0,
        alert_on_not_started = 1,
        details_retry_timeout_in_minutes = 40
WHERE scan_group='@SCAN_GROUP@';


commit;		/* NEVER REMOVE THIS LINE!!! */
