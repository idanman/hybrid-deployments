start transaction;		/* NEVER REMOVE THIS LINE!!! */

DELETE FROM ScanManager.tbl_scan_groups WHERE scan_group='@SCAN_GROUP@';
REPLACE INTO ScanManager.tbl_scan_groups
(scan_group,	is_active,	ontology_id,	type,		value) VALUES
('@SCAN_GROUP@',	1,		@ONTOLOGY@,		'sites',	'@SITES@');

commit;		/* NEVER REMOVE THIS LINE!!! */