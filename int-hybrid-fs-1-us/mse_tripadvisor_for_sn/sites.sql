start transaction;		/* NEVER REMOVE THIS LINE!!! */


-- WATCH OUT!
-- THERE IS A BUG IN SCAN-MANAGER THAT MAKES THE AUTO-BACKUP AND PURGE COMMANDS
-- RUN ON THE FIRST LINE ONLY.
-- HENCE, IF YOU HAVE SEVERAL SITES FROM THE SAME ONTOLOGY, DON'T CREATE SEVERAL LINES.
-- INSTEAD, USE ONE LINE, AND IN FIELD 'value' specify all the sites (comma separated).
DELETE FROM ScanManager.tbl_scan_groups WHERE scan_group='mse_tripadvisor_for_sn';
REPLACE INTO ScanManager.tbl_scan_groups
(scan_group,			is_active,	ontology_id,	type,		value		 ) VALUES
('mse_tripadvisor_for_sn',	1,			1399,			'sites',	'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,61,65,67');

commit;		/* NEVER REMOVE THIS LINE!!! */
