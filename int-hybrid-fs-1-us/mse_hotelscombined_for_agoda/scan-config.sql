start transaction;		/* NEVER REMOVE THIS LINE!!! */

REPLACE INTO ScanManager.tbl_scan_configs (scan_group) VALUES ('mse_hotelscombined_for_agoda');
UPDATE ScanManager.tbl_scan_configs SET
	customer_name='customer_ovrs_supernova_int',
	is_active = 0,
	scan_type = 'detailed',
	scan_pages = 1,
	when_hour = '25',
	export = 0,
	upload = 0,
	alert = 0,
	purge_records = 1,
	debug = 10,
	auto_from_backup = 0,

	-- Set this to 0 to disable check for not-started scans
	-- I.e. if disabled, the system will NOT insist on scheduling scan if it missed its start time
	alert_on_not_started = 1,
    details_retry_timeout_in_minutes = 30
WHERE scan_group='mse_hotelscombined_for_agoda';


commit;		/* NEVER REMOVE THIS LINE!!! */
