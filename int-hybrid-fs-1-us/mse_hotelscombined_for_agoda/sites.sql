start transaction;		/* NEVER REMOVE THIS LINE!!! */


-- WATCH OUT!
-- THERE IS A BUG IN SCAN-MANAGER THAT MAKES THE AUTO-BACKUP AND PURGE COMMANDS
-- RUN ON THE FIRST LINE ONLY.
-- HENCE, IF YOU HAVE SEVERAL SITES FROM THE SAME ONTOLOGY, DON'T CREATE SEVERAL LINES.
-- INSTEAD, USE ONE LINE, AND IN FIELD 'value' specify all the sites (comma separated).
DELETE FROM ScanManager.tbl_scan_groups WHERE scan_group='mse_hotelscombined_for_agoda';
REPLACE INTO ScanManager.tbl_scan_groups
(scan_group,			is_active,	ontology_id,	type,		value		 ) VALUES
('mse_hotelscombined_for_agoda',	1,			1707,			'sites',	'1,2,3,5,6,8,9,10,11,14,15,16,17,18,20,21,22,25,26,4,12,13,19,23,24,27,31,49,55,57,38,7,28,29,30,32,33,34,35,36,39,41,42,43,47,48,54,56,62,67');

commit;		/* NEVER REMOVE THIS LINE!!! */
