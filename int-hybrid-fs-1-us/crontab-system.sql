REPLACE INTO ScanManager.tbl_crontab (name) VALUES ('cronjob-beat');
UPDATE ScanManager.tbl_crontab SET
	is_active = 1,
	contact_emails = 'dmitry@fornova.net',
	when_minute = '*/20',
	when_hour = '*',
	when_day_of_month = '*',
	when_month = '*',
	when_day_of_week = '*',
	cmd = 'CALL ScanManager.SM_SCHEDULE_CRONJOB(0, "cronjob-beat", "echo 1", UTC_TIMESTAMP())',
	`type` = 'SQL'
WHERE name='cronjob-beat';

REPLACE INTO ScanManager.tbl_crontab (name) VALUES ('sm-clean');
UPDATE ScanManager.tbl_crontab SET
	is_active = 1,
	contact_emails = 'sparks-travel-prod@fornova.net',
	when_minute = '0',
	when_hour = '12',
	when_day_of_month = '*',
	when_month = '*',
	when_day_of_week = '*',
	cmd = 'CALL ScanManager.SM_SCHEDULE_CRONJOB(0, "sm-clean", "udmtools sm-clean -d 14 -V --skip-optimize ", UTC_TIMESTAMP())',
	`type` = 'SQL'
WHERE name='sm-clean';


COMMIT;
